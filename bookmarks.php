<?php
/**
 * Plugin Name: Bookmarks
 * Version: 1.0.0
 * Author: Max Medar
 * License: GPL2
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 *
 * Topper is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * any later version.
 *
 * Topper is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Topper. If not, see https://www.gnu.org/licenses/gpl-2.0.html.
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

define( "PLUGIN_URL", dirname( __FILE__ ) );
define( "PLUGIN_ADMIN_URL", PLUGIN_URL . '/admin' );
define( "PLUGIN_ADMIN_VIEWS_URL", PLUGIN_ADMIN_URL . '/views' );

require_once( PLUGIN_URL . '/includes/class-bookmarks.php' );

add_action( 'plugins_loaded', 'm_bookmarks_init' );

function m_bookmarks_init() {

	$bookmark = WP_Bookmarks::singleton( get_current_user_id() );
	$bookmark->run();

	if ( ! is_user_logged_in()) :
		add_filter( 'wp_nav_menu_objects', 'm_wp_nav_menu_objects', 10, 2 );
	endif;
}

/*
 * Hide bookmark menu item from anonymous users
 */
function m_wp_nav_menu_objects( $items, $args ) {

	foreach ( $items as &$item ) :
		if ( strpos( $item->url, 'm_bookmark' ) ) :
			unset( $items[ $item->menu_order ] );
		endif;
	endforeach;

	return $items;

}