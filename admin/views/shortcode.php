<ul class="m-bookmarks-list shortcode">
	<?php
	$bookmarks = $this->get_user_bookmarks();
	if ($bookmarks) :
	foreach ( $bookmarks  as $bookmark ) :
		?>
		<li id="m_post-<?php echo $bookmark; ?>" class="m-bookmark">
			<div class="m-remove-wrapper">
				<a href="#" class="m-remove" data-post-id="<?php echo $bookmark; ?>" title="Remove bookmark">
					<i class="fas fa-trash-alt"></i>
				</a>
			</div>
			<?php if ( has_post_thumbnail( $bookmark ) ) : ?>
				<a class="m-thumbnail-wrapper" href="<?php echo get_the_permalink( $bookmark ); ?>">
					<?php echo get_the_post_thumbnail( $bookmark, 'thumbnail' ); ?>
				</a>
			<?php endif; ?>
			<div class="m-content-wrapper">
				<h3>
					<a href="<?php echo get_the_permalink( $bookmark ); ?>">  <?php echo get_the_title( $bookmark ); ?></a>
				</h3>
				<div class="m-content">
					<?php
					$content = strip_shortcodes( get_post_field( 'post_content', $bookmark ) );
					$excerpt = wp_trim_words( $content, $num_words = 30, $more = null );
					echo $excerpt;
					?>
				</div>
			</div>
		</li>
	<?php
	endforeach;
	else :
		echo "<p>No bookmarks.</p>";
	endif;
	?>
</ul>
