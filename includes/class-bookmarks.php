<?php

class WP_Bookmarks {

	private static $instance;

	private $user_id;
	private $user_bookmarks = array();

	public static function singleton( $user_id ) {
		if ( ! isset( self::$instance ) ) :
			self::$instance = new self( $user_id );
		endif;

		return self::$instance;
	}

	private function __construct( $user_id ) {

		if ( $user_id ) :
			$this->user_id        = $user_id;
			$this->user_bookmarks = $this->load_user_bookmarks( $this->get_user_id() );

			add_action( 'wp_ajax_add_to_bookmarks', array( $this, '_add_to_bookmarks_cb' ) );
			add_action( 'wp_ajax_remove_from_bookmarks', array( $this, '_remove_from_bookmarks_cb' ) );
			add_action( 'wp_enqueue_scripts', array( $this, '_enqueue_scripts_and_styles' ) );
			add_action( 'admin_enqueue_scripts', array( $this, '_enqueue_scripts_and_styles' ) );

			add_action( 'init', array( $this, '_shortcodes_init' ) );
		endif;
	}

//	public function __destruct() {
//		$this->save_user_bookmarks( $this->get_user_id(), $this->get_user_bookmarks() );
//	}

	public function get_user_id() {
		return $this->user_id;
	}

	/*
	 * Load user bookmarks from db
	 */
	private function load_user_bookmarks( $user_id ) {
		$bookmarks = get_user_meta(
			$user_id,
			'_m_bookmarks',
			true
		);
		if ( $bookmarks ) :
			return $bookmarks;
		else:
			return array();
		endif;
	}

	/*
	 * Save user bookmarks to db
	 */
	private function save_user_bookmarks( $user_id, $bookmarks ) {
		return update_user_meta(
			$user_id,
			'_m_bookmarks',
			$bookmarks
		);
	}

	/*
	 * Check if bookmark already exist in bookmarks list
	 */
	private function is_bookmarked( $post_id ) {
		return in_array( $post_id, $this->get_user_bookmarks() );
	}

	/*
	 * Save bookmark to bookmarks list
	 */
	private function add_user_bookmark( $post_id ) {
		if ( ! $this->is_bookmarked( $post_id ) ) :
			array_push( $this->user_bookmarks, $post_id );

			$this->save_user_bookmarks( $this->get_user_id(), $this->get_user_bookmarks() );

			return true;
		endif;

		return false;
	}

	/*
	 * Remove bookmark from bookmarks list
	 */
	private function remove_user_bookmark( $post_id ) {
		if ( ( $key = array_search( $post_id, $this->user_bookmarks ) ) !== false ) {
			unset( $this->user_bookmarks[ $key ] );

			$this->save_user_bookmarks( $this->get_user_id(), $this->get_user_bookmarks() );

			return true;
		}

		return false;
	}

	/*
	 * Get all user bookmarks
	 */
	private function get_user_bookmarks() {
		return $this->user_bookmarks;
	}

	public function run() {
		if ( is_admin() ) :
			$this->run_admin();
		else :
			$this->run_public();
		endif;
	}

	private function run_admin() {
		add_action( 'admin_menu', array( $this, '_create_menu' ) );

	}

	private function run_public() {
		add_action( 'loop_start', array( $this, '_condition_filter_title' ) );
		add_action( 'loop_end', array( $this, '_condition_filter_title_remove' ) );


	}

	/*
	 * Filter to add bookmark icon to title
	 */
	public function _condition_filter_title() {
		add_filter( 'the_title', array( $this, '_add_btn' ), 10 );
	}

	/*
     * Filter to add bookmark icon to title
     */
	public function _condition_filter_title_remove() {
		remove_filter( 'the_title', array( $this, '_add_btn' ), 10 );
	}

	/*
	 * Callback for add btn to save bookmark
	 */
	public function _add_btn( $content ) {
		return $this->get_add_bookmark_btn_html() . $content;
	}

	/*
	 * Render save bookmark btn
	 */
	public function get_add_bookmark_btn_html() {
		$btn     = '';
		$post_id = get_the_ID();
//		if ( is_archive() || is_single() ) :
		if ( ! $this->is_bookmarked( $post_id ) ) :
			$btn = "<span class='m-add-to-bookmarks' data-post-id='$post_id' title='Add to bookmarks'></span>";
		else:
			$btn = "<span class=\"m-remove-from-bookmarks\" data-post-id=\"$post_id\" title=\"Remove from bookmarks\"></span>";
		endif;

//		endif;

		return $btn;
	}

	/*
	 * Callback for add plugin menu item
	 */
	public function _create_menu() {
		add_menu_page(
			'Bookmarks',
			'Bookmarks',
			'manage_options',
			'bookmarks',
			array( $this, '_options_html' )
		);
	}

	/*
	 * Render plugin options page in admin
	 */
	public function _options_html() {
		require_once( PLUGIN_ADMIN_VIEWS_URL . "/options-page.php" );
	}

	public function _shortcodes_init() {
		add_shortcode( 'm_bookmarks', array( $this, '_shortcode_bookmark' ) );
	}

	public function _shortcode_bookmark() {
		remove_filter( 'the_title', array( $this, '_add_btn' ), 10 );
		require_once( PLUGIN_ADMIN_VIEWS_URL . "/shortcode.php" );

	}

	public function _enqueue_scripts_and_styles() {
		wp_enqueue_style(
			'bookmarks-style',
			plugin_dir_url( __FILE__ ) . '../assets/css/style.css',
			array( 'font-awesome-style' ),
			'1.0.0',
			'all'
		);

		wp_enqueue_style(
			'font-awesome-style',
			'https://use.fontawesome.com/releases/v5.3.1/css/all.css',
			array(),
			'5.3.1',
			'all'
		);

		wp_enqueue_script(
			'bookmarks-public-js',
			plugin_dir_url( __FILE__ ) . '../assets/js/public.js',
			array( 'jquery' ),
			'1.0.0',
			true
		);
		wp_localize_script( 'bookmarks-public-js', 'add_to_bookmarks_ajax',
			array(
				'ajaxurl' => admin_url( 'admin-ajax.php' ),
				'nonce'   => wp_create_nonce( 'add_to_bookmarks_ajax-nonce' )
			)
		);

		wp_localize_script( 'bookmarks-public-js', 'remove_from_bookmarks_ajax',
			array(
				'ajaxurl' => admin_url( 'admin-ajax.php' ),
				'nonce'   => wp_create_nonce( 'remove_from_bookmarks_ajax-nonce' )
			)
		);
	}

	/*
	 * Ajax callbacks
	 */
	public function _add_to_bookmarks_cb() {
		check_ajax_referer( 'add_to_bookmarks_ajax-nonce', 'nonce_code' );
		$post_id  = $_POST['post_id'];
		$is_added = $this->add_user_bookmark( $post_id );
		if ( $is_added ) :
			echo 'success';
		else :
			echo 'fail';
		endif;
		wp_die();
	}

	public function _remove_from_bookmarks_cb() {
		check_ajax_referer( 'remove_from_bookmarks_ajax-nonce', 'nonce_code' );
		$post_id    = $_POST['post_id'];
		$is_removed = $this->remove_user_bookmark( $post_id );
		if ( $is_removed ) :
			echo 'success';
		else :
			echo 'fail';
		endif;
		wp_die();
	}

	/*
	 * Dev heplers
	 */
	static function dumper( $obj ) {
		echo
		"<pre>",
		htmlspecialchars( self::dumperGet( $obj ) ),
		"</pre>";
	}

	static function dumperGet( &$obj, $leftSp = "" ) {
		if ( is_array( $obj ) ) {
			$type = "Array[" . count( $obj ) . "]";
		} elseif ( is_object( $obj ) ) {
			$type = "Object";
		} elseif ( gettype( $obj ) == "boolean" ) {
			return $obj ? "true" : "false";
		} else {
			return "\"$obj\"";
		}
		$buf    = $type;
		$leftSp .= "    ";
		for ( Reset( $obj ); list( $k, $v ) = each( $obj ); ) {
			if ( $k === "GLOBALS" ) {
				continue;
			}
			$buf .= "\n$leftSp$k => " . self::dumperGet( $v, $leftSp );
		}

		return $buf;
	}

}