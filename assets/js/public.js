(function ($) {
    $(".m-add-to-bookmarks").on('click', add_too_bookmarks_cb);

    $(".m-remove").on('click', remove_from_bookmarks_admin_cb);

    $(".m-remove-from-bookmarks").on('click', remove_from_bookmarks_public_cb);

    function add_too_bookmarks_cb() {
        event.preventDefault();
        let $current_btn = $(this);
        let post_id = $current_btn.data('post-id');
        let data = {
            action: "add_to_bookmarks",
            nonce_code : add_to_bookmarks_ajax.nonce,
            post_id: post_id
        };

        $.post(add_to_bookmarks_ajax.ajaxurl, data, function (response) {
            if ('success' === response) {
                $current_btn.removeClass('m-add-to-bookmarks').addClass('m-remove-from-bookmarks').attr("title","Remove from bookmarks").on('click', remove_from_bookmarks_public_cb);
            } else if ('fail' === response) {

            }
        });
    }

    function remove_from_bookmarks_admin_cb() {
        event.preventDefault();
        let post_id = $(this).data('post-id');
        let data = {
            action: "remove_from_bookmarks",
            nonce_code : remove_from_bookmarks_ajax.nonce,
            post_id: post_id
        };

        $.post(remove_from_bookmarks_ajax.ajaxurl, data, function (response) {
            console.log(response);
            if ('success' === response) {
                console.log("here!");
                $("#m_post-" + post_id).fadeOut(300, function () {
                    $(this).remove();
                });
            } else if ('fail' === response) {
                $("#m_post-" + post_id).addClass('m-warning');

            }
        });
    }

    function remove_from_bookmarks_public_cb() {
        event.preventDefault();
        let $current_btn = $(this);
        let post_id = $current_btn.data('post-id');
        let data = {
            action: "remove_from_bookmarks",
            nonce_code : remove_from_bookmarks_ajax.nonce,
            post_id: post_id
        };

        $.post(remove_from_bookmarks_ajax.ajaxurl, data, function (response) {
            if ('success' === response) {
                $current_btn.removeClass('m-remove-from-bookmarks').addClass('m-add-to-bookmarks').attr("title", "Add to bookmarks").on('click', add_too_bookmarks_cb);
            } else if ('fail' === response) {

            }
        });
    }

} )( jQuery );